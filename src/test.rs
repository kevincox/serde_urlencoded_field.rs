use serde_derive::{Deserialize, Serialize};

#[derive(Debug, Deserialize, PartialEq, Serialize)]
struct Test {
	#[serde(with="crate::default")]
	bytes: Vec<u8>,
	#[serde(with="crate::default")]
	string: String,
}

#[derive(Serialize)]
struct TestSerlialize<'a> {
	#[serde(with="crate::default")]
	byte_slice: &'a [u8],
	#[serde(with="crate::default")]
	str_slice: &'a str,
}

#[test]
fn test_all() {
	let data = Test {
		bytes: b"foo/bar<baz>".to_vec(),
		string: "Foo?".to_string(),
	};

	let s = serde_json::to_string_pretty(&data).unwrap();
	assert_eq!(s, r#"{
  "bytes": "foo/bar%3Cbaz%3E",
  "string": "Foo%3F"
}"#);

	let roundtrip: Test = serde_json::from_str(&s).unwrap();
	assert_eq!(roundtrip, data);
}

#[test]
fn test_serialize_only() {
	let data = TestSerlialize {
		byte_slice: b"$ and &",
		str_slice: "\0\n",
	};

	let s = serde_json::to_string_pretty(&data).unwrap();
	assert_eq!(s, r#"{
  "byte_slice": "$%20and%20&",
  "str_slice": "%00%0A"
}"#);
}
